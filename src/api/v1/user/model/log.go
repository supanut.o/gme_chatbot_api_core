package model

//===> Define Struct Log Event Msg
type LogEventMsgStruct struct {
	ObjectID    int
	UserID      string
	LogEventMsg string
}

//{"UserID":"U89e6c8335ce16c05b9d77b0c09e8cc2c","GroupID":"C64d4916d6bde1ca4cd6c57096d37349a","RoomID":"","Name":"","Department":"","Position":""
//,"Email":"","AuthenStatus":0,"EventMsg":"gข","LogEventMsg":"event.Message"}
