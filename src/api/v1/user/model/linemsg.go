package model

///=====>>> เอาไว้ Get From DB
type MessageLine struct {
	UserID   string
	EventMsg string
}

///=====>>> เอาไว้ Get From DB
type DataForBatchLogFlexMsg struct {
	BatchName       string
	BatchStatus     string
	BatchTextDetail string
}

///=====>>> เอาไว้ Get From DB
type DataForBatchGLDetailFlexMsg struct {
	BatchName   string
	BatchStart  string
	BatchEnd    string
	BatchStatus string
}
