package model

//===> Define Struct Log Event Msg
type Result struct {
	ResultCode int
	ResultMsg  string
	KEY1       int
	KEY2       string
}
