package model

///=====>>> เอาไว้บันทึกลง DB
type MailMsgForSaveDB struct {
	SeqSet       int
	MsgUid       string
	MsgDate      string
	MsgSubject   string
	MsgFrom      string
	MsgSender    string
	MsgReplyTo   string
	MsgTo        string
	MsgCc        string
	MsgBcc       string
	MsgInReplyTo string
	MsgMessageId string
	MsgBody      string
}

///=====>>> เอาไว้แสดงผลตอนดึงค่าออกมาจาก DB
type MailMsgFetchMailView struct {
	ObjectID       int
	MsgUid         string
	MsgDate        string
	MsgSubject     string
	MsgMessageId   string
	ResultDescView string
}
