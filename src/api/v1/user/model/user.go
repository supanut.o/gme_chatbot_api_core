package model

//===> Define Struct User
type BackupUser struct {
	ObjectID     int
	LineID       string
	LineName     string
	UserID       string
	AuthenStatus int
}

type User struct {
	ObjectID    int    `json:"ObjectID"`
	LineID      string `json:"LineID"`
	LineName    string `json:"LineName"`
	UserID      string `json:"UserID"`
	GroupID     string `json:"GroupID"`
	RoomID      string `json:"RoomID"`
	Name        string `json:"Name"`
	Department  string `json:"Department"`
	Position    string `json:"Position"`
	Email       string `json:"Email"`
	EventMsg    string `json:"EventMsg"`
	LogEventMsg string `json:"LogEventMsg"`

	AuthenStatus     int    `json:"AuthenStatus"`
	AuthenStatusDesc string `json:"AuthenStatusDesc"`
}

//{"UserID":"U89e6c8335ce16c05b9d77b0c09e8cc2c","GroupID":"C64d4916d6bde1ca4cd6c57096d37349a","RoomID":"","Name":"","Department":"","Position":""
//,"Email":"","AuthenStatus":0,"EventMsg":"gข","LogEventMsg":"event.Message"}

//===> Define Struct UserRegister
type UserRegister struct {
	ObjectID     int    `json:"objectID"`
	UserID       string `json:"userID"`
	Name         string `json:"name"`
	Department   string `json:"department"`
	Position     string `json:"position"`
	Email        string `json:"email"`
	AuthenStatus int    `json:"authenStatus"`
	LogEventMsg  string `json:"logEventMsg"`
}

//===> Define Struct UserRegisterRes
type UserRegisterRes struct {
	ObjectID     int `json:"objectID"`
	LineID       string
	LineName     string
	UserID       string
	Name         string
	Department   string
	Position     string
	Email        string
	AuthenStatus int
	LogEventMsg  string
	CreateDate   string
	UpdateDate   string
}

//===> Define Struct UserAuthen
type UserAuthen struct {
	UserID string `json:"UserID"`
	//IsActive     int
	AuthenStatus     int    `json:"AuthenStatus"`
	AuthenStatusDesc string `json:"AuthenStatusDesc"`
}

//===> Define Struct UserAuthen
type UserResult struct {
	ObjectID   int    `json:"ObjectID"`
	UserID     string `json:"UserID"`
	ResultCode int    `json:"ResultCode"`
	ResultMsg  string `json:"ResultMsg"`
}
