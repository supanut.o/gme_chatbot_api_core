package datasource

import (
	//import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"encoding/json"
	"log"
	"strings"
	//_ "github.com/denisenkom/go-mssqldb"
	model "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/* #region GETDATA_MAPPING_BYEVENTMSG */
//============================ METHOD FOR GETDATA MAPPING BY EVENTMSG ============================//
func GetDataMappingByEventMsgDAO(_mMessageLine model.MessageLine, jsonStr string) (int, error, string) {
	log.Println()
	log.Println(string("=== BEGIN GetDataMappingByEventMsgDAO() ==="))
	conn := dbConn()
	defer conn.Close()

	var db *sql.DB
	db = conn
	rows, err := db.Query("call CB_GETDATA_MAPPING_BYEVENTMSG_SELECT_PROC(?, ?)", _mMessageLine.UserID, strings.ToUpper(strings.TrimSpace(_mMessageLine.EventMsg)))
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()

	DataForBatchLogFlexMsgs := []model.DataForBatchLogFlexMsg{}
	var _dFlexMsg model.DataForBatchLogFlexMsg
	count := 0
	for rows.Next() {
		//err := rows.Scan(&ObjectID, &LineId, &LineName, &UserID, &AuthenStatus) //===> Loop Scan to variable with Check Error
		errrow := rows.Scan(&_dFlexMsg.BatchName, &_dFlexMsg.BatchStatus, &_dFlexMsg.BatchTextDetail) //===> Loop Scan to Object Struct with Check Error
		if errrow != nil {
			log.Println("Error reading rows: " + errrow.Error())
			return -1, errrow, "Error reading rows: " + errrow.Error()
		}
		log.Printf("BatchName: %s, BatchStatus: %s, BatchTextDetail: %s\n", _dFlexMsg.BatchName, _dFlexMsg.BatchStatus, _dFlexMsg.BatchTextDetail)
		DataForBatchLogFlexMsgs = append(DataForBatchLogFlexMsgs, _dFlexMsg) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	jsonData, _ := json.Marshal(DataForBatchLogFlexMsgs)
	// Convert bytes to string.
	jsonDataStr := string(jsonData)
	log.Println(string("=== END GetDataMappingByEventMsgDAO() ==="))
	log.Println()
	return count, err, jsonDataStr
}

//========================================================================================//
/* #endregion */

/* #region GETDETAIL_BATCHGL_BYBATCHNAME */
//============================ METHOD FOR GETDETAIL BATCHGL BY BATCHNAME ============================//
func GetDetailBatchGL_ByBatchNameDAO(_mMessageLine model.MessageLine, jsonStr string) (int, error, string) {
	log.Println()
	log.Println(string("=== BEGIN GetDetailBatchGL_ByBatchNameDAO() ==="))
	conn := dbConn()
	defer conn.Close()

	var db *sql.DB
	db = conn
	rows, err := db.Query("call CB_GETDETAIL_BATCHGL_BYBATCHNAME_SELECT_PROC(?, ?)", _mMessageLine.UserID, _mMessageLine.EventMsg)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()

	DataForBatchGLDetailFlexMsgs := []model.DataForBatchGLDetailFlexMsg{}
	var _dFlexMsg model.DataForBatchGLDetailFlexMsg
	count := 0
	for rows.Next() {
		errrow := rows.Scan(&_dFlexMsg.BatchName, &_dFlexMsg.BatchStart, &_dFlexMsg.BatchEnd, &_dFlexMsg.BatchStatus) //===> Loop Scan to Object Struct with Check Error
		if errrow != nil {
			log.Println("Error reading rows: " + errrow.Error())
			return -1, errrow, "Error reading rows: " + errrow.Error()
		}
		log.Printf("BatchName: %s, BatchStart: %s, BatchEnd: %s, BatchStatus: %s\n", _dFlexMsg.BatchName, _dFlexMsg.BatchStart, _dFlexMsg.BatchEnd, _dFlexMsg.BatchStatus)
		DataForBatchGLDetailFlexMsgs = append(DataForBatchGLDetailFlexMsgs, _dFlexMsg) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	jsonData, _ := json.Marshal(DataForBatchGLDetailFlexMsgs)
	// Convert bytes to string.
	jsonDataStr := string(jsonData)
	log.Println(string("=== END GetDetailBatchGL_ByBatchNameDAO() ==="))
	log.Println()
	return count, err, jsonDataStr
}

//========================================================================================//
/* #endregion */
