package datasource

import (
	"encoding/json" //import package json.NewEncode
	"fmt"

	// import package net-http เข้ามา
	"database/sql"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"
	model "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/*
var (
	server   = "remotemysql.com"
	port     = "3306"
	user     = "WTGJ0SNurT"
	password = "7ZhwgEGLHg"
	database = "WTGJ0SNurT"
)

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := user
	dbPass := password
	dbName := database
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+server+":"+port+")/"+dbName)
	//db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		log.Println("connect fail")
		log.Fatal("Open connection failed:", err.Error())
		panic(err.Error())
	} else {
		log.Println("connect success")
	}
	return db
} */

//var tmpl = template.Must(template.ParseGlob("form/*"))

/* //====================>>>>>>>>> BEGIN SAMPLE
func Index(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM Employee ORDER BY id DESC")
	if err != nil {
		panic(err.Error())
	}
	emp := Employee{}
	res := []Employee{}
	for selDB.Next() {
		var id int
		var name, city string
		err = selDB.Scan(&id, &name, &city)
		if err != nil {
			panic(err.Error())
		}
		emp.Id = id
		emp.Name = name
		emp.City = city
		res = append(res, emp)
	}
	tmpl.ExecuteTemplate(w, "Index", res)
	defer db.Close()
}

func Show(w http.ResponseWriter, r *http.Request) {
	db := dbConn()
	nId := r.URL.Query().Get("id")
	selDB, err := db.Query("SELECT * FROM Employee WHERE id=?", nId)
	if err != nil {
		panic(err.Error())
	}
	emp := Employee{}
	for selDB.Next() {
		var id int
		var name, city string
		err = selDB.Scan(&id, &name, &city)
		if err != nil {
			panic(err.Error())
		}
		emp.Id = id
		emp.Name = name
		emp.City = city
	}
	tmpl.ExecuteTemplate(w, "Show", emp)
	defer db.Close()
}
//====================>>>>>>>>> END SAMPLE */

// DAO
func GetAllUserDAO() (int, error, string) {
	// Read employees
	fmt.Println(string("=== GetAllUserDAO() ==="))
	count, err, userDataStr := GetAllUser()
	return count, err, userDataStr
}

/* #region Function Database */
func GetAllUser() (int, error, string) {
	fmt.Println()
	fmt.Println(string("=== Begin GetAllUser() ==="))

	// Connect to database
	conn := dbConn()
	defer conn.Close()
	//=====>>> BEGIN TEST (4) : READ FROM DATABASE
	//defer conn.Close()

	// Read data
	count, err, userDataStr := GetUserDB(conn)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetAllUserDB failed:", err.Error())
	}
	fmt.Printf("Read %d rows successfully.\n", count)
	//=====>>> END TEST (4) : READ FROM DATABASE

	//w.WriteHeader(http.StatusOK)
	//w.Write([]byte(`{"message": "get called"}`))
	//w.Write([]byte(orgPositionDataStr))
	fmt.Println(string("=== End GetAllUser() ==="))

	return count, err, userDataStr
}

func GetUserDB(db *sql.DB) (int, error, string) {
	fmt.Println()
	fmt.Println(string("=============== BEGIN (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))

	//db := dbConn()
	tsql := fmt.Sprintf("SELECT * FROM CB_LINEUSER_AUTHEN ORDER BY ObjectID ASC")
	rows, err := db.Query(tsql)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectId id ASC")
	if err != nil {
		fmt.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	userStructs := []model.User{}
	count := 0
	for rows.Next() {
		var _userStruct model.User
		var LineID, LineName, UserID string
		var ObjectID, AuthenStatus int
		err := rows.Scan(&ObjectID, &LineID, &LineName, &UserID, &AuthenStatus)                                                              //===> Loop Scan to variable with Check Error
		err2 := rows.Scan(&_userStruct.ObjectID, &_userStruct.LineID, &_userStruct.LineName, &_userStruct.UserID, &_userStruct.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			fmt.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		if err2 != nil {
			fmt.Println("Error reading rows: " + err2.Error())
			return -1, err2, "Error reading rows: " + err.Error()
		}
		fmt.Printf("ObjectId: %d, LineId: %s, LineName: %s, UserID: %s, AuthenStatus: %d\n", ObjectID, LineID, LineName, UserID, AuthenStatus)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct

		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(userStructs)
	// Convert bytes to string.
	userDataStr := string(userData)
	//fmt.Println(orgPositionDataStr)
	fmt.Println(string("=============== END (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))
	fmt.Println()

	return count, nil, userDataStr
}

func GetUserByObjectIdDB(db *sql.DB, uid int) (int, error, string) {
	fmt.Println()
	fmt.Println(string("=============== BEGIN (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))

	//db := dbConn()
	tsql := fmt.Sprintf("SELECT * FROM CB_LINEUSER_AUTHEN ORDER BY ObjectId ASC")
	rows, err := db.Query(tsql)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectId id ASC")
	if err != nil {
		fmt.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	userStructs := []model.User{}
	count := 0
	for rows.Next() {
		var _userStruct model.User
		var LineID, LineName, UserID string
		var ObjectID, AuthenStatus int
		err := rows.Scan(&ObjectID, &LineID, &LineName, &UserID, &AuthenStatus)                                                              //===> Loop Scan to variable with Check Error
		err2 := rows.Scan(&_userStruct.ObjectID, &_userStruct.LineID, &_userStruct.LineName, &_userStruct.UserID, &_userStruct.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			fmt.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		if err2 != nil {
			fmt.Println("Error reading rows: " + err2.Error())
			return -1, err2, "Error reading rows: " + err.Error()
		}
		fmt.Printf("ObjectID: %d, LineID: %s, LineName: %s, UserID: %s, AuthenStatus: %d\n", ObjectID, LineID, LineName, UserID, AuthenStatus)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct

		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(userStructs)
	// Convert bytes to string.
	userDataStr := string(userData)
	//fmt.Println(orgPositionDataStr)
	fmt.Println(string("=============== END (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))
	fmt.Println()

	return count, nil, userDataStr
}

/* #endregion */

// DAO
func GetUserPostDAO(UserID string) (int, error, string) {
	// Read employees
	fmt.Println(string("=== GetAllUserDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := GetUserPostDB(conn, UserID)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetAllUserDB failed:", err.Error())
	}
	fmt.Printf("Read %d rows successfully.\n", count)

	return count, err, userDataStr
}

func GetUserPostDB(db *sql.DB, UserID string) (int, error, string) {
	fmt.Println()
	fmt.Println(string("=============== BEGIN (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))

	//db := dbConn()
	tsql := fmt.Sprintf("SELECT * FROM CB_LINEUSER_AUTHEN WHERE UserID='" + UserID + "'")
	rows, err := db.Query(tsql)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectId id ASC")
	if err != nil {
		fmt.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	userStructs := []model.User{}
	count := 0
	for rows.Next() {
		var _userStruct model.User
		var LineID, LineName, UserID string
		var ObjectID, AuthenStatus int
		err := rows.Scan(&ObjectID, &LineID, &LineName, &UserID, &AuthenStatus)                                                              //===> Loop Scan to variable with Check Error
		err2 := rows.Scan(&_userStruct.ObjectID, &_userStruct.LineID, &_userStruct.LineName, &_userStruct.UserID, &_userStruct.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			fmt.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		if err2 != nil {
			fmt.Println("Error reading rows: " + err2.Error())
			return -1, err2, "Error reading rows: " + err.Error()
		}
		fmt.Printf("ObjectID: %d, LineID: %s, LineName: %s, UserID: %s, AuthenStatus: %d\n", ObjectID, LineID, LineName, UserID, AuthenStatus)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct

		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(userStructs)
	// Convert bytes to string.
	userDataStr := string(userData)
	//fmt.Println(orgPositionDataStr)
	fmt.Println(string("=============== END (GetAllUserDB): READ FROM DATABASE (LISTENING) ==============="))
	fmt.Println()

	return count, nil, userDataStr
}

/* #region CHECK_USER_STATUS_AuthenStatus */
//============================ METHOD FOR CHECK USER PROFILE ============================//
func GetCheckUserAuthenStatusDAO(UserID string) (int, error, string) {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := GetCheckUserAuthenStatusDB(conn, UserID)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetCheckUserAuthenStatusDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr
}
func GetCheckUserAuthenStatusDB(db *sql.DB, pUserID string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (GetCheckUserAuthenStatusDB): READ FROM DATABASE (LISTENING) ==============="))

	tsql := fmt.Sprintf("SELECT ObjectId, UserID, AuthenStatus FROM CB_LINEUSER_AUTHEN WHERE UserID='" + pUserID + "'")
	rows, err := db.Query(tsql)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectId id ASC")
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	userStructs := []model.User{}
	var _userStruct model.User
	//var LineId, LineName, UserID string
	var _UserID string
	var _ObjectID, _AuthenStatus, _Status int
	_Status = 0
	count := 0
	for rows.Next() {

		err := rows.Scan(&_ObjectID, &_UserID, &_AuthenStatus)                                   //===> Loop Scan to variable with Check Error
		err2 := rows.Scan(&_userStruct.ObjectID, &_userStruct.UserID, &_userStruct.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		if err2 != nil {
			log.Println("Error reading rows: " + err2.Error())
			return -1, err2, "Error reading rows: " + err2.Error()
		}
		log.Printf("ObjectID: %d, UserID: %s, AuthenStatus: %d\n", _userStruct.ObjectID, _userStruct.UserID, _userStruct.AuthenStatus)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct
		count++
	}

	//===> For Check Authen User With Status
	if count > 0 {
		if _AuthenStatus == 0 {
			_Status = 1
		} else if _AuthenStatus == 1 {
			_Status = 2
		} else if _AuthenStatus == 2 {
			_Status = 3
		} else if _AuthenStatus == 3 {
			_Status = 4
		}
	} else {
		_UserID = pUserID
	}
	var _UserAuthen = model.UserAuthen{UserID: _UserID, AuthenStatus: _Status}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(_UserAuthen)
	// Convert bytes to string.
	userAuthenStatusDataStr := string(userData)
	log.Println(string("=============== END (GetCheckUserAuthenStatusDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, userAuthenStatusDataStr
}

func GetCheckUserAuthenStatusDAO2(_mUser model.User) (int, error, string) {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusDAO2() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := GetCheckUserAuthenStatusDB2(conn, _mUser)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetCheckUserAuthenStatusDAO2 failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr
}
func GetCheckUserAuthenStatusDB2(db *sql.DB, _mUser model.User) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (GetCheckUserAuthenStatusDB2): READ FROM DATABASE (LISTENING) ==============="))

	rows, err := db.Query("call CB_LINEUSER_CHECKAUTHEN_PROC(?, ?, ?, ?)", _mUser.UserID, _mUser.GroupID, _mUser.RoomID, _mUser.EventMsg)

	log.Println("_mUser.UserID=", _mUser.UserID, "_mUser.GroupID=", _mUser.GroupID, ",_mUser.RoomID=", _mUser.RoomID, ",_mUser.EventMsg=", _mUser.EventMsg)

	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	userStructs := []model.User{}
	var _userStruct model.User
	//var LineId, LineName, UserID string
	var _UserID, _AuthenStatusDesc string
	var _ObjectID, _AuthenStatus int
	count := 0
	for rows.Next() {

		err := rows.Scan(&_ObjectID, &_UserID, &_AuthenStatus, &_AuthenStatusDesc)                                              //===> Loop Scan to variable with Check Error
		err2 := rows.Scan(&_userStruct.ObjectID, &_userStruct.UserID, &_userStruct.AuthenStatus, &_userStruct.AuthenStatusDesc) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		if err2 != nil {
			log.Println("Error reading rows: " + err2.Error())
			return -1, err2, "Error reading rows: " + err2.Error()
		}
		log.Printf("ObjectID: %d, UserID: %s, AuthenStatus: %d, AuthenStatusDesc: %s\n", _userStruct.ObjectID, _userStruct.UserID, _userStruct.AuthenStatus, _userStruct.AuthenStatusDesc)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct
		count++
	}
	// -1 = Connection error
	// 0 = User not registered
	// 1 = Waiting to process
	// 2 = Active
	// 3 = Lock usage
	// 4 = Waiting to unlock
	// 99 = Bot Sleep

	var _UserAuthen = model.UserAuthen{UserID: _UserID, AuthenStatus: _AuthenStatus, AuthenStatusDesc: _AuthenStatusDesc}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(_UserAuthen)
	// Convert bytes to string.
	userAuthenStatusDataStr := string(userData)
	log.Println(string("=============== END (GetCheckUserAuthenStatusDB2): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, userAuthenStatusDataStr
}

//===>>> FOR TEST
func GetCheckUserAuthenStatusDAOTest(UserID string) (int, error, string, int) {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusDAOTest() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr, userAuthenStatus := GetCheckUserAuthenStatusDBTest(conn, UserID)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetCheckUserAuthenStatusDAOTest failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr, userAuthenStatus
}
func GetCheckUserAuthenStatusDBTest(db *sql.DB, UserID string) (int, error, string, int) {
	log.Println()
	log.Println(string("=============== BEGIN (GetCheckUserAuthenStatusDBTest): READ FROM DATABASE (LISTENING) ==============="))

	tsql := fmt.Sprintf("SELECT AuthenStatus FROM CB_LINEUSER_AUTHEN WHERE UserID='" + UserID + "'")
	rows, err := db.Query(tsql)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectId id ASC")
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error(), 0
	}
	defer rows.Close()
	userStructs := []model.User{}
	var _userStruct model.User
	//var LineId, LineName, UserID string
	//var ObjectId, AuthenStatus int
	var AuthenStatus int
	count := 0
	for rows.Next() {

		err := rows.Scan(&AuthenStatus) //===> Loop Scan to variable with Check Error
		//err := rows.Scan(&_userStruct.ObjectId, &_userStruct.UserID, &_userStruct.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error(), 0
		}
		log.Printf("ObjectID: %d, UserID: %s, AuthenStatus: %d\n", _userStruct.ObjectID, _userStruct.UserID, _userStruct.AuthenStatus)
		userStructs = append(userStructs, _userStruct) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	//userData, _ := json.Marshal(userStructs)
	// Convert bytes to string.
	//userAuthenStatusDataStr := string(userData)
	userAuthenStatusDataStr := string(AuthenStatus)
	log.Println(AuthenStatus)
	log.Println(userAuthenStatusDataStr)
	log.Println(string("=============== END (GetCheckUserAuthenStatusDBTest): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, userAuthenStatusDataStr, AuthenStatus
}

//========================================================================================//
/* #endregion */

/* #region GET_LINE_USER_AUTHEN_BY_ALL */
//============================ METHOD GET LINE USER AUTHEN BY ALL ============================//
func GetUserAuthenAllDAO(UserID string) (int, error, string) {
	log.Println()
	log.Println(string("=== GetUserAuthenAllDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := GetUserAuthenAllDB(conn, UserID)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetUserAuthenAllDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")
	return count, err, userDataStr
}
func GetUserAuthenAllDB(db *sql.DB, pUserID string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (GetUserAuthenAllDB) ==============="))
	rows, err := db.Query("call CB_LINEUSER_GETALL_SELECT_PROC(?)", pUserID)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	Users := []model.User{}
	var _User model.User
	count := 0
	for rows.Next() {
		err := rows.Scan(&_User.ObjectID, &_User.LineID, &_User.LineName, &_User.UserID, &_User.GroupID, &_User.RoomID, &_User.Name, &_User.Department, &_User.Position, &_User.Email, &_User.EventMsg, &_User.LogEventMsg, &_User.AuthenStatus, &_User.AuthenStatusDesc)
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Println("Read ", count, " rows successfully.")
		Users = append(Users, _User) //===> Loop Append To Object Struct
		count++
	}
	//===> Begin Manage Structs To JSON String
	jsonData, _ := json.Marshal(Users)
	// Convert bytes to string.
	jsonDataStr := string(jsonData)
	log.Println(string("=============== END (GetUserAuthenAllDB) ==============="))
	log.Println()
	return count, nil, jsonDataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_LINE_USER_AUTHEN_BY_ID */
//============================ METHOD GET LINE USER AUTHEN BY ID ============================//
func GetUserAuthenByIdDAO(UserID string, ObjectID int) (int, error, string) {
	log.Println()
	log.Println(string("=== GetUserAuthenByIdDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := GetUserAuthenByIdDB(conn, UserID, ObjectID)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetUserAuthenByIdDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")
	return count, err, userDataStr
}
func GetUserAuthenByIdDB(db *sql.DB, pUserID string, pObjectID int) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (GetUserAuthenByIdDB) ==============="))
	rows, err := db.Query("call CB_LINEUSER_GETBYID_SELECT_PROC(?, ?)", pUserID, pObjectID)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	Users := []model.User{}
	var _User model.User
	count := 0
	for rows.Next() {
		err := rows.Scan(&_User.ObjectID, &_User.LineID, &_User.LineName, &_User.UserID, &_User.GroupID, &_User.RoomID, &_User.Name, &_User.Department, &_User.Position, &_User.Email, &_User.EventMsg, &_User.LogEventMsg, &_User.AuthenStatus, &_User.AuthenStatusDesc)
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Println("Read ", count, " rows successfully.")
		Users = append(Users, _User) //===> Loop Append To Object Struct
		count++
	}
	//===> Begin Manage Structs To JSON String
	jsonData, _ := json.Marshal(Users)
	// Convert bytes to string.
	jsonDataStr := string(jsonData)
	log.Println(string("=============== END (GetUserAuthenByIdDB) ==============="))
	log.Println()
	return count, nil, jsonDataStr
}

//========================================================================================//
/* #endregion */

/* #region UPDATE_USER_PROFILE_STATUS */
//============================ METHOD FOR UPDATE USER PROFILE STATUS ============================//
func UpdateUserProfileStatusDAO(UserID string, ObjectID int, StatusType int, StatusTypeText string) (int, error, string) {
	log.Println()
	log.Println(string("=== UpdateUserProfileStatusDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := UpdateUserProfileStatusDB(conn, UserID, ObjectID, StatusType, StatusTypeText)
	_ = userDataStr
	if err != nil {
		log.Fatal("GetUserAuthenByIdDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")
	return count, err, userDataStr
}
func UpdateUserProfileStatusDB(db *sql.DB, pUserID string, pObjectID int, pStatusType int, pStatusTypeText string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (UpdateUserProfileStatusDB) ==============="))
	rows, err := db.Query("call CB_LINEUSER_STATUS_UPDATE_PROC(?, ?, ?, ?)", pUserID, pObjectID, pStatusType, pStatusTypeText)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	Results := []model.Result{}
	var _Result model.Result
	count := 0
	for rows.Next() {
		err := rows.Scan(&_Result.ResultCode, &_Result.ResultMsg, &_Result.KEY1) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ResultCode: %d, ResultMsg: %s, KEY1: %d\n", _Result.ResultCode, _Result.ResultMsg, _Result.KEY1)
		Results = append(Results, _Result) //===> Loop Append To Object Struct
		count++
	}
	//===> Begin Manage Structs To JSON String
	jsonData, _ := json.Marshal(Results)
	jsonDataStr := string(jsonData)
	log.Println(string("=============== END (UpdateUserProfileStatusDB) ==============="))
	log.Println()

	return count, nil, jsonDataStr
}

//========================================================================================//
/* #endregion */
