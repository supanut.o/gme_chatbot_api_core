package datasource

import (
	//import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"encoding/json"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"
	model "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/* #region GET_MAIL_WITH_IAMP */
//============================ METHOD FOR GET MAIL WITH IMAP ============================//
func InsertMailWithImapDAO(_mMailMsg model.MailMsgForSaveDB) (int, error, string) {
	log.Println()
	log.Println(string("=== InsertMailWithImapDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, dataStr := InsertMailWithImapDB(conn, _mMailMsg)
	_ = dataStr
	if err != nil {
		log.Fatal("InsertMailWithImapDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, dataStr
}
func InsertMailWithImapDB(db *sql.DB, _mMailMsg model.MailMsgForSaveDB) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (InsertMailWithImapDB): READ FROM DATABASE (LISTENING) ==============="))
	rows, err := db.Query("call CB_FETCHMAIL_INSERT_PROC(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", _mMailMsg.MsgUid, _mMailMsg.MsgDate, _mMailMsg.MsgSubject, _mMailMsg.MsgFrom, _mMailMsg.MsgSender, _mMailMsg.MsgReplyTo, _mMailMsg.MsgTo, _mMailMsg.MsgCc, _mMailMsg.MsgBcc, _mMailMsg.MsgInReplyTo, _mMailMsg.MsgMessageId, _mMailMsg.MsgBody)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	Results := []model.Result{}
	var _Result model.Result
	count := 0
	for rows.Next() {

		//err := rows.Scan(&ObjectID, &LineId, &LineName, &UserID, &AuthenStatus) //===> Loop Scan to variable with Check Error
		err := rows.Scan(&_Result.ResultCode, &_Result.ResultMsg, &_Result.KEY1) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ResultCode: %d, ResultMsg: %s, KEY1: %d\n", _Result.ResultCode, _Result.ResultMsg, _Result.KEY1)
		Results = append(Results, _Result) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	jsonData, _ := json.Marshal(Results)
	// Convert bytes to string.
	jsonDataStr := string(jsonData)
	log.Println(string("=============== END (InsertMailWithImapDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, jsonDataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_MAIL_DATA_FROM_DATABASE (LAST EVENT BY EVENTMSG) */
//============================ METHOD FOR GET MAIL DATA FROM DATABASE (LAST EVENT BY EVENTMSG) ============================//
func RequestLastMailDataByEventMsgDAO(_pUserID string, _pEventMsg string) (int, error, string) {
	log.Println()
	log.Println(string("=== RequestLastMailDataByEventMsgDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, resDataStr := RequestLastMailDataByEventMsgDB(conn, _pUserID, _pEventMsg)
	if err != nil {
		log.Fatal("RequestLastMailDataByEventMsgDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, resDataStr
}
func RequestLastMailDataByEventMsgDB(db *sql.DB, _pUserID string, _pEventMsg string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (RequestLastMailDataByEventMsgDB): READ FROM DATABASE (LISTENING) ==============="))

	//===> ยังไม่ได้ทำ Store
	rows, err := db.Query("call CB_FETCHMAIL_LASTEVENT_SELECT_PROC(?, ?)", _pUserID, _pEventMsg)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	MailMsgFetchMailViews := []model.MailMsgFetchMailView{}
	var _MailMsgFetchMailView model.MailMsgFetchMailView

	count := 0
	for rows.Next() {
		err := rows.Scan(&_MailMsgFetchMailView.ObjectID, &_MailMsgFetchMailView.MsgUid, &_MailMsgFetchMailView.MsgDate, &_MailMsgFetchMailView.MsgSubject, &_MailMsgFetchMailView.MsgMessageId, &_MailMsgFetchMailView.ResultDescView)
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ObjectID: %d, MsgUid: %s, MsgDate: %s, MsgSubject: %s, MsgMessageId: %s, ResultDescView: %s\n", _MailMsgFetchMailView.ObjectID, _MailMsgFetchMailView.MsgUid, _MailMsgFetchMailView.MsgDate, _MailMsgFetchMailView.MsgSubject, _MailMsgFetchMailView.MsgMessageId, _MailMsgFetchMailView.ResultDescView)
		MailMsgFetchMailViews = append(MailMsgFetchMailViews, _MailMsgFetchMailView) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	resData, _ := json.Marshal(MailMsgFetchMailViews)
	// Convert bytes to string.
	resDataStr := string(resData)
	log.Println(string("=============== END (RequestLastMailDataByEventMsgDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, resDataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_MAIL_DATA_FROM_DATABASE (ALL EVENT BY EVENTMSG) */
//============================ METHOD FOR GET MAIL DATA FROM DATABASE (ALL EVENT BY EVENTMSG) ============================//
func RequestAllMailDataByEventMsgDAO(_pUserID string, _pEventMsg string) (int, error, string) {
	log.Println()
	log.Println(string("=== RequestAllMailDataByEventMsgDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, resDataStr := RequestAllMailDataByEventMsgDB(conn, _pUserID, _pEventMsg)
	if err != nil {
		log.Fatal("RequestAllMailDataByEventMsgDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, resDataStr
}
func RequestAllMailDataByEventMsgDB(db *sql.DB, _pUserID string, _pEventMsg string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (RequestAllMailDataByEventMsgDB): READ FROM DATABASE (LISTENING) ==============="))

	//===> ยังไม่ได้ทำ Store
	rows, err := db.Query("call CB_FETCHMAIL_ALLEVENT_SELECT_PROC(?, ?)", _pUserID, _pEventMsg)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	MailMsgFetchMailViews := []model.MailMsgFetchMailView{}
	var _MailMsgFetchMailView model.MailMsgFetchMailView

	count := 0
	for rows.Next() {
		err := rows.Scan(&_MailMsgFetchMailView.ObjectID, &_MailMsgFetchMailView.MsgUid, &_MailMsgFetchMailView.MsgDate, &_MailMsgFetchMailView.MsgSubject, &_MailMsgFetchMailView.MsgMessageId, &_MailMsgFetchMailView.ResultDescView)
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ObjectID: %d, MsgUid: %s, MsgDate: %s, MsgSubject: %s, MsgMessageId: %s, ResultDescView: %s\n", _MailMsgFetchMailView.ObjectID, _MailMsgFetchMailView.MsgUid, _MailMsgFetchMailView.MsgDate, _MailMsgFetchMailView.MsgSubject, _MailMsgFetchMailView.MsgMessageId, _MailMsgFetchMailView.ResultDescView)
		MailMsgFetchMailViews = append(MailMsgFetchMailViews, _MailMsgFetchMailView) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	resData, _ := json.Marshal(MailMsgFetchMailViews)
	// Convert bytes to string.
	resDataStr := string(resData)
	log.Println(string("=============== END (RequestAllMailDataByEventMsgDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, resDataStr
}

//========================================================================================//
/* #endregion */
