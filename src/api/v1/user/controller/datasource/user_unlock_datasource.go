package datasource

import (
	"encoding/json" //import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"
	modelUser "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/* #region UNLOCK_USER_MEMBER */
//============================ METHOD FOR UNLOCK USER MEMBER ============================//
func RequestUserUnLockDAO(_pUserID string, _pReason string) (int, error, string) {
	log.Println()
	log.Println(string("=== RequestUserUnLockDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := RequestUserUnLockDB(conn, _pUserID, _pReason)
	_ = userDataStr
	if err != nil {
		log.Fatal("RequestUserUnLockDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr
}
func RequestUserUnLockDB(db *sql.DB, _pUserID string, _pReason string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (RequestUserUnLockDB): READ FROM DATABASE (LISTENING) ==============="))

	rows, err := db.Query("call CB_LINEUSER_REQUESTUNLOCK_PROC(?, ?)", _pUserID, _pReason)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	UserResults := []modelUser.UserResult{}
	var _UserResult modelUser.UserResult

	count := 0
	for rows.Next() {
		err := rows.Scan(&_UserResult.ObjectID, &_UserResult.UserID, &_UserResult.ResultCode, &_UserResult.ResultMsg) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ObjectID: %d, UserID: %s, ResultCode: %d, ResultMsg: %s\n", _UserResult.ObjectID, _UserResult.UserID, _UserResult.ResultCode, _UserResult.ResultMsg)
		UserResults = append(UserResults, _UserResult) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(UserResults)
	// Convert bytes to string.
	userDataStr := string(userData)
	log.Println(string("=============== END (RequestUserUnLockDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, userDataStr
}

//========================================================================================//
/* #endregion */
