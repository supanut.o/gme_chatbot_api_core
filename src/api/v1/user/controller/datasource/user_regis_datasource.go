package datasource

import (
	"encoding/json" //import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"
	modelUser "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/* #region REGISTER_LINE_CHATBOT_USER */
//============================ METHOD FOR REGISTER LINE CHATBOT USER PROFILE ============================//
func RegisterLineChatbotUserDAO(_uRegister modelUser.UserRegister) (int, error, string) {
	log.Println()
	log.Println(string("=== RegisterLineChatbotUserDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := RegisterLineChatbotUserDB(conn, _uRegister)
	_ = userDataStr
	if err != nil {
		log.Fatal("RegisterLineChatbotUserDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr
}
func RegisterLineChatbotUserDB(db *sql.DB, _uRegister modelUser.UserRegister) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (RegisterLineChatbotUserDB): READ FROM DATABASE (LISTENING) ==============="))

	//tsql := fmt.Sprintf("SELECT ObjectID, UserID, AuthenStatus FROM CB_LINEUSER_AUTHEN WHERE UserID='" + "UserID" + "'")
	rows, err := db.Query("call CB_LINEUSER_REGISTER_PROC(?, ?, ?, ?, ?, ?)", _uRegister.UserID, _uRegister.Name, _uRegister.Department, _uRegister.Position, _uRegister.Email, _uRegister.LogEventMsg)
	//rows, err := db.Query("SELECT * FROM CB_LINEUSER_AUTHEN ORDER ObjectID id ASC")
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	UserRegisters := []modelUser.UserRegisterRes{}
	var _UserRegister modelUser.UserRegisterRes
	//var LineId, LineName, UserID string
	//var ObjectID, AuthenStatus int

	count := 0
	for rows.Next() {

		//err := rows.Scan(&ObjectID, &LineId, &LineName, &UserID, &AuthenStatus) //===> Loop Scan to variable with Check Error
		err := rows.Scan(&_UserRegister.ObjectID, &_UserRegister.UserID, &_UserRegister.Name, &_UserRegister.Department, &_UserRegister.Position, &_UserRegister.Email, &_UserRegister.AuthenStatus) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ObjectID: %d, UserID: %s, Name: %s, Department: %s, Position: %s, Email: %s, AuthenStatus: %d, LogEventMsg: %s\n", _UserRegister.ObjectID, _UserRegister.UserID, _UserRegister.Name, _UserRegister.Department, _UserRegister.Position, _UserRegister.Email, _UserRegister.AuthenStatus, _UserRegister.LogEventMsg)
		UserRegisters = append(UserRegisters, _UserRegister) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	userData, _ := json.Marshal(UserRegisters)
	// Convert bytes to string.
	userDataStr := string(userData)
	log.Println(string("=============== END (RegisterLineChatbotUserDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, userDataStr
}

//========================================================================================//
/* #endregion */
