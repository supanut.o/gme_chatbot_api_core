package datasource

import (
	//import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"

	_ "github.com/go-sql-driver/mysql"
)

var (
	server   = "127.0.0.1"
	port     = "3306"
	user     = "root"
	password = "P@ssw0rd"
	database = "gme_chatbot"
)

/* var (
	server   = "remotemysql.com"
	port     = "3306"
	user     = "WTGJ0SNurT"
	password = "7ZhwgEGLHg"
	database = "WTGJ0SNurT"
) */

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := user
	dbPass := password
	dbName := database
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+server+":"+port+")/"+dbName)
	//db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		log.Println("connect fail")
		log.Fatal("Open connection failed:", err.Error())
		panic(err.Error())
	} else {
		log.Println("connect success")
	}
	return db
}
