package datasource

import (
	//import package json.NewEncode

	// import package net-http เข้ามา
	"database/sql"
	"encoding/json"
	"log"

	//_ "github.com/denisenkom/go-mssqldb"
	model "../../model"
	_ "github.com/go-sql-driver/mysql"
)

/* #region LINE_LOG_EVENT_MSG */
//============================ METHOD FOR REGISTER LINE CHATBOT USER PROFILE ============================//
func InsertLogEventChatbotDAO(_mLogEventMsg model.LogEventMsgStruct, jsonStr string) (int, error, string) {
	log.Println()
	log.Println(string("=== InsertLogEventChatbotDAO() ==="))
	conn := dbConn()
	defer conn.Close()
	count, err, userDataStr := InsertLogEventChatbotDB(conn, _mLogEventMsg, jsonStr)
	_ = userDataStr
	if err != nil {
		log.Fatal("InsertLogEventChatbotDAO failed:", err.Error())
	}
	log.Println("Read ", count, " rows successfully.")

	return count, err, userDataStr
}
func InsertLogEventChatbotDB(db *sql.DB, _mLogEventMsg model.LogEventMsgStruct, jsonStr string) (int, error, string) {
	log.Println()
	log.Println(string("=============== BEGIN (InsertLogEventChatbotDB): READ FROM DATABASE (LISTENING) ==============="))
	rows, err := db.Query("call CB_LINEUSER_LOGEVENT_INSERT_PROC(?, ?)", _mLogEventMsg.UserID, jsonStr)
	if err != nil {
		log.Println("Error reading rows: " + err.Error())
		return -1, err, "Error reading rows: " + err.Error()
	}
	defer rows.Close()
	LogEventMsgStructs := []model.LogEventMsgStruct{}
	var _LogEventMsgStruct model.LogEventMsgStruct
	count := 0
	for rows.Next() {

		//err := rows.Scan(&ObjectID, &LineId, &LineName, &UserID, &AuthenStatus) //===> Loop Scan to variable with Check Error
		err := rows.Scan(&_LogEventMsgStruct.ObjectID, &_LogEventMsgStruct.UserID, &_LogEventMsgStruct.LogEventMsg) //===> Loop Scan to Object Struct with Check Error
		if err != nil {
			log.Println("Error reading rows: " + err.Error())
			return -1, err, "Error reading rows: " + err.Error()
		}
		log.Printf("ObjectID: %d, UserID: %s, LogEventMsg: %s\n", _LogEventMsgStruct.ObjectID, _LogEventMsgStruct.UserID, _LogEventMsgStruct.LogEventMsg)
		LogEventMsgStructs = append(LogEventMsgStructs, _LogEventMsgStruct) //===> Loop Append To Object Struct
		count++
	}

	//===> Begin Manage Structs To JSON String
	//fmt.Println()
	logData, _ := json.Marshal(LogEventMsgStructs)
	// Convert bytes to string.
	logDataStr := string(logData)
	log.Println(string("=============== END (RegisterLineChatbotUserDB): READ FROM DATABASE (LISTENING) ==============="))
	log.Println()

	return count, nil, logDataStr
}

//========================================================================================//
/* #endregion */
