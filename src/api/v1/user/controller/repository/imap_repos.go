package repository

import (
	datasource "../../controller/datasource"
	model "../../model"

	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"strconv"
	"time"

	mail "github.com/emersion/go-message/mail"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"

	"github.com/labstack/echo"
	"gopkg.in/gomail.v2"
)

var constHost = "imap.gmail.com:993"
var constUser = "xxxx"
var constPass = "xxxx"
var constReadMailBox = "GME_MAIL"

///====================>>> ADDD NEW FUNCTION
type ServerGmail struct {
	user    string
	pass    string
	erro    string
	cliente *client.Client
}

/* ///=====>>> เอาไว้บันทึกลง DB
type MailMsgForSaveDB struct {
	SeqSet       int
	MsgUid       string
	MsgDate      string
	MsgSubject   string
	MsgFrom      string
	MsgSender    string
	MsgReplyTo   string
	MsgTo        string
	MsgCc        string
	MsgBcc       string
	MsgInReplyTo string
	MsgMessageId string
	MsgBody      string
} */

func panicHandler() {
	x := recover()
	if x == "nodatamail" {
		log.Println("No Data Mail UnRead!")
	}
}

/* #region GET_MAIL_WITH_IAMP */
//============================ METHOD FOR GET MAIL WITH IMAP ============================//
func RequestMailImapBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RequestMailImapBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	//===> STEP 1: GET MAIL AND INSERT TO DATABASE
	dataStr := processGetMailWithImap()
	//dataStr := "Done!"

	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}

//========================================================================================//
/* #endregion */

func processGetMailWithImap() string {

	//===> TEST

	serverGmail := NewServerGmail()
	serverGmail.Connect()
	serverGmail.Login()
	result := serverGmail.ListUnseenMessages()
	if result == "" {
		result = "{}"
	}
	log.Println(result)
	log.Println("Done!")
	return result

}

func NewServerGmail() *ServerGmail {
	serverGmail := &ServerGmail{}
	serverGmail.user = constUser
	serverGmail.pass = constPass
	serverGmail.erro = ""

	return serverGmail
}

func (serverGmail *ServerGmail) Connect() {
	// Connect to server
	cliente, erro := client.DialTLS(constHost, nil)
	if erro != nil {
		serverGmail.erro = erro.Error()
	}
	log.Println("Connected")

	serverGmail.cliente = cliente

}

func (serverGmail *ServerGmail) Login() {
	// Login
	if erro := serverGmail.cliente.Login(serverGmail.user, serverGmail.pass); erro != nil {
		serverGmail.erro = erro.Error()
	}
	log.Println("Logged")

}

func (serverGmail *ServerGmail) setLabelBox(label string) *imap.MailboxStatus {
	//mailbox, erro := serverGmail.cliente.Select(label, true)
	mailbox, erro := serverGmail.cliente.Select(label, false)
	if erro != nil {
		serverGmail.erro = erro.Error()
	}
	log.Println("Flags for ", label, ": ", mailbox.Flags)
	log.Println("All Mail Message: ", mailbox.Messages)
	return mailbox
}

func (serverGmail *ServerGmail) ListUnseenMessages() string {
	defer panicHandler()
	// set mailbox to INBOX
	//serverGmail.setLabelBox("INBOX")
	serverGmail.setLabelBox(constReadMailBox)

	// criteria to search for unseen messages
	criteria := imap.NewSearchCriteria()
	criteria.WithoutFlags = []string{"\\Seen"}

	uids, err := serverGmail.cliente.UidSearch(criteria)
	if err != nil {
		log.Println(err)
	}

	seqSet := new(imap.SeqSet)
	log.Println(seqSet)
	/* //===> วิธีการ Return
	go func() {
		if seqSet.Empty() {
			log.Println("Not Exists Mail is UnRead on MailBox.")
			return
		}
	}() */
	seqSet.AddNum(uids...)
	log.Println(uids)
	log.Println(seqSet)
	if seqSet.Empty() {
		_Result := model.Result{ResultCode: 1, ResultMsg: "Not Exists Mail is UnRead on MailBox.", KEY1: 0, KEY2: ""}
		jsonData, _ := json.Marshal(_Result)
		jsonDataStr := string(jsonData)
		log.Println("Not Exists Mail is UnRead on MailBox.")
		return jsonDataStr
	}
	section := &imap.BodySectionName{}
	items := []imap.FetchItem{imap.FetchEnvelope, imap.FetchFlags, imap.FetchInternalDate, section.FetchItem()}
	_ = items
	messages := make(chan *imap.Message)
	go func() {
		if err := serverGmail.cliente.UidFetch(seqSet, items, messages); err != nil {
			//log.Fatal(err)
			//log.Panic("nodatamail")
			log.Println(err)
			return
			//log.Panic(err)
		}
	}()

	MailMsgForSaveDBs := []model.MailMsgForSaveDB{}
	var _MailMsgForSaveDB model.MailMsgForSaveDB
	_ = _MailMsgForSaveDB
	var resDataStr = ""

	for message := range messages {
		// ใช้ struct MailMsgForSaveDB สำหรับบันทึกลง DB
		//_envMessageId := message.Envelope.MessageId
		//log.Println(">> MsgMessageId: ", _envMessageId)
		if message.Envelope == nil {
			break
			//log.Fatal("Server didn't returned message")
		}
		res_Subject, res_Date, res_From, res_Sender, res_ReplyTo, res_To, res_Cc, res_Bcc, res_InReplyTo, res_MessageId := GetInfoMailMessage(message.Envelope)

		log.Println(">> MsgUid: ", message.Uid)
		log.Println(">> res_subject: ", res_Subject)
		log.Println(">> res_date: ", res_Date)
		log.Println(">> res_From: ", res_From)
		log.Println(">> res_Sender: ", res_Sender)
		log.Println(">> res_ReplyTo: ", res_ReplyTo)
		log.Println(">> res_To: ", res_To)
		log.Println(">> res_Cc: ", res_Cc)
		log.Println(">> res_Bcc: ", res_Bcc)
		log.Println(">> res_InReplyTo: ", res_InReplyTo)
		log.Println(">> res_MessageId: ", res_MessageId)

		_messageUidInt := int(message.Uid)
		_messageUidStr := strconv.FormatInt(int64(message.Uid), 10) //string(message.Uid)
		//_ = _messageUidInt
		//_ = _messageUidStr
		// GET BODY
		if message == nil {
			//log.Fatal("Server didn't returned message")
			//log.Panic("Server didn't returned message")
			_Result := model.Result{ResultCode: 1, ResultMsg: "Server didn't returned message.", KEY1: 0, KEY2: ""}
			jsonData, _ := json.Marshal(_Result)
			jsonDataStr := string(jsonData)
			log.Println("Server didn't returned message.")
			return jsonDataStr
		}

		r := message.GetBody(section)
		if r == nil {
			//log.Fatal("Server didn't returned message body")
			//log.Panic("Server didn't returned message body")
			_Result := model.Result{ResultCode: 1, ResultMsg: "Server didn't returned message body.", KEY1: 0, KEY2: ""}
			jsonData, _ := json.Marshal(_Result)
			jsonDataStr := string(jsonData)
			log.Println("Server didn't returned message body.")
			return jsonDataStr
		}
		// Create a new mail reader
		mr, err := mail.CreateReader(r)
		//defer mr.Close()
		if err != nil {
			//log.Fatal(err)
			//log.Panic(err)
			log.Println(err)
			return err.Error()
		}
		//// Print some info about the message
		//header := mr.Header
		//_ = header
		p, err := mr.NextPart()

		if err == io.EOF {
			break
		} else if err != nil {
			//log.Fatal(err)
			log.Println(err)
			return err.Error()
		}

		var res_Body = ""
		switch h := p.Header.(type) {
		case *mail.InlineHeader:
			b, _ := ioutil.ReadAll(p.Body)
			//log.Printf("Got text: %v\n", string(b))
			res_Body = string(b)
			log.Println(">> res_MessageId: ", string(res_Body))
		case *mail.AttachmentHeader:
			filename, _ := h.Filename()
			log.Printf("Got attachment: %v\n", filename)
		}

		//===>> LOOP TO STRUCT
		_MailMsgForSaveDB = model.MailMsgForSaveDB{SeqSet: _messageUidInt, MsgUid: _messageUidStr, MsgDate: res_Date, MsgSubject: res_Subject, MsgFrom: res_From, MsgSender: res_Sender, MsgReplyTo: res_ReplyTo, MsgTo: res_To, MsgCc: res_Cc, MsgBcc: res_Bcc, MsgInReplyTo: res_InReplyTo, MsgMessageId: res_MessageId, MsgBody: res_Body}
		//_MailMsgForSaveDB = MailMsgForSaveDB{SeqSet: _messageUidInt, MsgUid: _messageUidStr, MsgDate: "res_Date", MsgSubject: "res_Subject", MsgFrom: "res_From", MsgSender: "res_Sender", MsgReplyTo: "res_ReplyTo", MsgTo: "res_To", MsgCc: "res_Cc", MsgBcc: "res_Bcc", MsgInReplyTo: "res_InReplyTo", MsgMessageId: "res_MessageId", MsgBody: "res_Body"}
		MailMsgForSaveDBs = append(MailMsgForSaveDBs, _MailMsgForSaveDB) //===> Loop Append To Object Struct

		count, err, dataStr := datasource.InsertMailWithImapDAO(_MailMsgForSaveDB)
		log.Println("count : ", count, " - err : ", err, " - dataStr : ", err)
		resDataStr = dataStr
	}

	/* var _mMailMsg MailMsgForSaveDB
	for i, x := range MailMsgForSaveDBs {
		//x.MsgUid = bar // doom. useless assignment to local variable x.
		log.Println(">> i: ", i, ", x", x.MsgMessageId)
		_mMailMsg = x
		count, err, dataStr := datasource.InsertMailWithImapDAO(_mMailMsg)
		log.Println("count : ", count, " - err : ", err, " - dataStr : ", err)
		resDataStr = dataStr
	} */
	return resDataStr
}

func GetInfoMailMessage(envParam *imap.Envelope) (res_subject string, res_date string, res_From string, res_Sender string, res_ReplyTo string, res_To string, res_Cc string, res_Bcc string, res_InReplyTo string, res_MessageId string) {
	/* // The message date.
	Date time.Time
	// The message subject.
	Subject string
	// The From header addresses.
	From []*Address
	// The message senders.
	Sender []*Address
	// The Reply-To header addresses.
	ReplyTo []*Address
	// The To header addresses.
	To []*Address
	// The Cc header addresses.
	Cc []*Address
	// The Bcc header addresses.
	Bcc []*Address
	// The In-Reply-To header. Contains the parent Message-Id.
	InReplyTo string
	// The Message-Id header.
	MessageId string */

	//===> Date
	var env *imap.Envelope = new(imap.Envelope)
	env = envParam
	_envDate := env.Date
	_envDateJson, errEnvDate := json.Marshal(_envDate)
	if errEnvDate != nil {
		fmt.Println(errEnvDate)
	}
	//==
	if _envDateJson != nil {
		res_date = _envDate.Format("2006-01-02 15:04:05")
	} else {
		res_date = ""
	}
	fmt.Println(string(_envDateJson))
	//log.Println("MsgDate: ", string(_envDateJson))
	fmt.Println("yyyy-mm-dd HH:mm:ss : ", _envDate)

	//===> Subject
	_envSubject := env.Subject
	_envSubjectJson, errEnvSubject := json.Marshal(_envSubject)
	if errEnvSubject != nil {
		fmt.Println(errEnvSubject)
	}
	//==
	if _envSubjectJson != nil {
		res_subject = string(_envSubjectJson)
	} else {
		res_subject = ""
	}
	//fmt.Println(string(_envSubjectJson))
	//log.Println("MsgSubject: ", string(_envSubjectJson))

	//===> From
	_envFrom := env.From
	_envFromJson, errEnvFrom := json.Marshal(_envFrom)
	if errEnvFrom != nil {
		fmt.Println(errEnvFrom)
	}
	//==
	if _envFromJson != nil {
		res_From = string(_envFromJson)
	} else {
		res_From = ""
	}
	fmt.Println(string(_envFromJson))
	//log.Println("MsgFrom: ", string(_envFromJson))

	//===> Sender
	_envSender := env.Sender
	_envSenderJson, errEnvSender := json.Marshal(_envSender)
	if errEnvSender != nil {
		fmt.Println(errEnvSender)
	}
	//==
	if _envSenderJson != nil {
		res_Sender = string(_envSenderJson)
	} else {
		res_Sender = ""
	}
	fmt.Println(string(_envSenderJson))
	//log.Println("MsgSender: ", string(_envSenderJson))

	//===> ReplyTo
	_envReplyTo := env.ReplyTo
	_envReplyToJson, errEnvReplyTo := json.Marshal(_envReplyTo)
	if errEnvReplyTo != nil {
		fmt.Println(errEnvReplyTo)
	}
	//==
	if _envReplyToJson != nil {
		res_ReplyTo = string(_envReplyToJson)
	} else {
		res_ReplyTo = ""
	}
	fmt.Println(string(_envReplyToJson))
	//log.Println("MsgReplyTo: ", string(_envReplyToJson))

	//===> To
	_envTo := env.To
	_envToJson, errEnvTo := json.Marshal(_envTo)
	if errEnvTo != nil {
		fmt.Println(errEnvTo)
	}
	//==
	if _envToJson != nil {
		res_To = string(_envToJson)
	} else {
		res_To = ""
	}
	fmt.Println(string(_envToJson))
	//log.Println("MsgTo: ", string(_envToJson))

	//===> Cc
	_envCc := env.Cc
	_envCcJson, errEnvCc := json.Marshal(_envCc)
	if errEnvCc != nil {
		fmt.Println(errEnvCc)
	}
	//==
	if (_envCcJson != nil) && (_envCc != nil) {
		res_Cc = string(_envCcJson)
	} else {
		res_Cc = ""
	}
	fmt.Println(string(_envCcJson))
	//log.Println("MsgCc: ", string(_envCcJson))

	//===> Bcc
	_envBcc := env.Bcc
	_envBccJson, errEnvBcc := json.Marshal(_envBcc)
	if errEnvBcc != nil {
		fmt.Println(errEnvBcc)
	}
	//==
	if (_envBccJson != nil) && (_envBcc != nil) {
		res_Bcc = string(_envBccJson)
	} else {
		res_Bcc = ""
	}
	fmt.Println(string(_envBccJson))
	//log.Println("MsgBcc: ", string(_envBccJson))

	//===> InReplyTo
	_envInReplyTo := env.InReplyTo
	_envInReplyToJson, errEnvInReplyTo := json.Marshal(_envInReplyTo)
	if errEnvInReplyTo != nil {
		fmt.Println(errEnvInReplyTo)
	}
	//==
	if _envInReplyToJson != nil {
		res_InReplyTo = string(_envInReplyToJson)
	} else {
		res_InReplyTo = ""
	}
	fmt.Println(string(_envInReplyToJson))
	//log.Println("MsgInReplyTo: ", string(_envInReplyToJson))

	//===> MessageId
	_envMessageId := env.MessageId
	_envMessageIdJson, errEnvMessageId := json.Marshal(_envMessageId)
	if errEnvMessageId != nil {
		fmt.Println(errEnvMessageId)
	}
	//==
	if _envMessageIdJson != nil {
		res_MessageId = string(_envMessageIdJson)
	} else {
		res_MessageId = ""
	}
	fmt.Println(string(_envMessageIdJson))
	//log.Println("MsgMessageId: ", string(_envMessageIdJson))

	return res_subject, res_date, res_From, res_Sender, res_ReplyTo, res_To, res_Cc, res_Bcc, res_InReplyTo, res_MessageId
	//string,  string,  string,  string,  string,  string,  string,  string,  string,  string
}

/* #region GET_MAIL_DATA_FROM_DATABASE (LAST EVENT BY EVENTMSG) */
//============================ METHOD FOR GET MAIL DATA FROM DATABASE (LAST EVENT BY EVENTMSG) ============================//
func RequestLastMailDataByEventMsgBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RequestLastMailDataByEventMsgBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID, _EventMsg string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "EventMsg" {
			_EventMsg = value.(string)
		}
	}

	count, err, dataStr := datasource.RequestLastMailDataByEventMsgDAO(_UserID, _EventMsg)
	log.Println("count : ", count, " - err : ", err)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_MAIL_DATA_FROM_DATABASE (ALL EVENT BY EVENTMSG) */
//============================ METHOD FOR GET MAIL DATA FROM DATABASE (ALL EVENT BY EVENTMSG) ============================//
func RequestAllMailDataByEventMsgBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RequestAllMailDataByEventMsgBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID, _EventMsg string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "EventMsg" {
			_EventMsg = value.(string)
		}
	}

	count, err, dataStr := datasource.RequestAllMailDataByEventMsgDAO(_UserID, _EventMsg)
	log.Println("count : ", count, " - err : ", err)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_DATA_FOR_SENDMAIL_LOG_BATCH */
//============================ METHOD FOR GET DATA FOR SENDMAIL LOG BATCH ============================//
func RequestBatchLogSendMailBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RequestBatchLogSendMailBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID, _BatchName, _EmailSend, _Description string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "BatchName" {
			_BatchName = value.(string)
		} else if key == "EmailSend" {
			_EmailSend = value.(string)
		} else if key == "Description" {
			_Description = value.(string)
		}
	}
	_ = _UserID
	_ = _BatchName
	_ = _EmailSend
	_ = _Description
	//count, err, dataStr := datasource.RequestAllMailDataByEventMsgDAO(_UserID, _EventMsg)
	var count int
	var err string
	var dataStr string
	dataStr = RequestBatchLogSendMailTestSenMail(_EmailSend, _BatchName, _Description)
	log.Println("count : ", count, " - err : ", err)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}
func RequestBatchLogSendMailTestSenMail(_pEmailSend string, _pBatchName string, _pDescription string) string {

	/*
		m := gomail.NewMessage()
		m.SetHeader("From", constUser)
		m.SetHeader("To", "supanut892@gmail.com")
		m.SetHeader("Subject", "Hello How!")
		//===> ADD EMBED IMAGE EMAIL
		//m.Embed("D0tkKb_VsAAHWsp.jpg")
		//m.Embed("p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg")
		//m.SetBody("text/html", `Hello you <br><br><img src="cid:D0tkKb_VsAAHWsp.jpg" alt="My image" /><br><img src="p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg" alt="My image" />`)
		m.SetBody("text/html", _pDescription)

		d := gomail.NewPlainDialer("smtp.gmail.com", 587, constUser, constPass)

		if err := d.DialAndSend(m); err != nil {
			//panic(err)
			log.Println(err)
		}

		//===
		m2 := gomail.NewMessage()
		m2.SetHeader("From", constUser)
		m2.SetHeader("To", "panuwat.pa@gmail.com")
		m2.SetHeader("Subject", "Hello How!")
		//===> ADD EMBED IMAGE EMAIL
		//m2.Embed("D0tkKb_VsAAHWsp.jpg")
		//m2.Embed("p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg")
		//m2.SetBody("text/html", `Hello you <br><br><img src="cid:D0tkKb_VsAAHWsp.jpg" alt="My image" /><br><img src="p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg" alt="My image" />`)
		m2.SetBody("text/html", _pDescription)
		d2 := gomail.NewPlainDialer("smtp.gmail.com", 587, constUser, constPass)
		if err := d2.DialAndSend(m2); err != nil {
			//panic(err)
			log.Println(err)
		}
	*/

	m3 := gomail.NewMessage()
	m3.SetHeader("From", constUser)
	m3.SetHeader("To", _pEmailSend)
	m3.SetHeader("Subject", _pBatchName)
	//===> ADD EMBED IMAGE EMAIL
	//m3.Embed("D0tkKb_VsAAHWsp.jpg")
	//m3.Embed("p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg")
	//m3.SetBody("text/html", _pDescription+` <br><br><img src="cid:D0tkKb_VsAAHWsp.jpg" alt="My image" /><br><img src="p-พริตตี้-ยิ้มหวาน-motor-expo-2017.jpg" alt="My image" />`)
	m3.SetBody("text/html", _pDescription)

	d3 := gomail.NewPlainDialer("smtp.gmail.com", 587, constUser, constPass)

	if err := d3.DialAndSend(m3); err != nil {
		//panic(err)
		log.Println(err)
	}

	/* // smtpServer data to smtp server
	type smtpServer struct {
		host string
		port string
	} // serverName URI to smtp server
	// Sender data.
	from := constUser
	password := constPass // Receiver email address.
	to := []string{
		"xxxx@gmail.com",
		"xxxxa@gmail.com",
	}
	// smtp server configuration.
	var _smtpServer smtpServer
	_ = _smtpServer
	_smtpServer = smtpServer{host: "smtp.gmail.com", port: "587"} // Message.
	//message := []byte("Hello, How are you?")
	message := []byte("To: zzz@gmail.com; zzz.pa@gmail.com\r\n" +
		"Subject: Hello, How are you?\r\n" +
		"\r\n" +
		"Here’s the space for our great aoi \r\n") // Authentication.
	auth := smtp.PlainAuth("", from, password, _smtpServer.host) // Sending email.
	err := smtp.SendMail("smtp.gmail.com:587", auth, from, to, message)
	if err != nil {
		fmt.Println(err)
		return
	} */

	_Result := model.Result{ResultCode: 0, ResultMsg: "Email Sent!", KEY1: 0, KEY2: ""}
	jsonData, _ := json.Marshal(_Result)
	jsonDataStr := string(jsonData)
	log.Println("Email Sent!")
	return jsonDataStr

}

//========================================================================================//
/* #endregion */
