package repository

import (
	"encoding/json"
	"log"

	dsUser "../../controller/datasource"
	modelUser "../../model"
)

//===>>> LOG
func InsertLogEventChatbot(jsonStr string) {
	log.Println()
	log.Println(string("=== Begin InsertLogEventChatbot() ==="))
	var LogEventMsgStruct modelUser.LogEventMsgStruct
	err := json.Unmarshal([]byte(jsonStr), &LogEventMsgStruct)
	count, err, dataStr := dsUser.InsertLogEventChatbotDAO(LogEventMsgStruct, jsonStr)
	if err != nil {
		panic(err)
	}
	_ = count
	_ = err
	_ = dataStr
	log.Println(string("=== End InsertLogEventChatbot() ==="))

}
