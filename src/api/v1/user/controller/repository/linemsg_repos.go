package repository

import (
	"encoding/json"
	"log"
	"math/big"
	"time"

	dsUser "../../controller/datasource"
	model "../../model"
	"github.com/labstack/echo"
)

func GetDataMappingByEventMsgBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetDataMappingByEventMsgBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}

	/* var _UserID, _EventMsg string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "EventMsg" {
			_EventMsg = value.(string)
		}
	} */

	var _MessageLine model.MessageLine
	err := json.Unmarshal([]byte(jsonStr), &_MessageLine)
	count, err, dataStr := dsUser.GetDataMappingByEventMsgDAO(_MessageLine, jsonStr)
	log.Println("count : ", count, " - err : ", err)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}

func GetDetailBatchGL_ByBatchNameBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetDetailBatchGL_ByBatchNameBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}

	var _MessageLine model.MessageLine
	err := json.Unmarshal([]byte(jsonStr), &_MessageLine)
	count, err, dataStr := dsUser.GetDetailBatchGL_ByBatchNameDAO(_MessageLine, jsonStr)
	log.Println("count : ", count, " - err : ", err)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}