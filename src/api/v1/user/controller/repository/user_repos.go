package repository

import (
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"strconv"
	"time"

	dsUser "../../controller/datasource"
	modelUser "../../model"
	"github.com/labstack/echo"
)

// BLL

func GetAllUserBLL() string {
	fmt.Println(string("=== GetAllUserBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Printf("Begin Main Start Time %s", start)
	fmt.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e
	count, err, userDataStr := dsUser.GetAllUserDAO()
	fmt.Println(count)
	fmt.Println(err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Printf("End Main Run took %s", elapsed)
	return userDataStr
}

func GetUserPostBLL(jsonStr string) string {
	fmt.Println(string("=== getUserPostBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Printf("Begin Main Start Time %s", start)
	fmt.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var LineName, UserID string
	for key, value := range lineReqMap {
		fmt.Println("index : ", key, " value : ", value)
		if key == "uid" {
			UserID = value.(string)
		} else if key == "name" {
			LineName = value.(string)
		}
	}
	log.Println("UserID Value : ", string(UserID))
	log.Println("LineName Value : ", string(LineName))

	count, err, userDataStr := dsUser.GetUserPostDAO(UserID)
	fmt.Println(count)
	fmt.Println(err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Printf("End Main Run took %s", elapsed)
	return userDataStr
}

/* #region CHECK_USER_STATUS_AuthenStatus */
//============================ METHOD FOR CHECK USER PROFILE ============================//
func GetCheckUserAuthenStatusBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	//===>>> CALL STATEMENT INSERT LOG ===<<<//
	log.Println(string("InsertLogEventChatbot: "), jsonStr)
	//InsertLogEventChatbot(jsonStr)

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}

	var User modelUser.User
	errUser := json.Unmarshal([]byte(jsonStr), &User)
	countUser, errUser, dataStr := dsUser.GetCheckUserAuthenStatusDAO2(User)
	log.Println("count : ", countUser, " - err : ", errUser)
	_ = dataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return dataStr
}

func GetCheckUserAuthenStatusBLL_V1(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	//===>>> CALL STATEMENT INSERT LOG ===<<<//
	log.Println(string("InsertLogEventChatbot: "), jsonStr)
	//InsertLogEventChatbot(jsonStr)

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var LineName, UserID, GroupID, RoomID, EventMsg, LogEventMsg string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			UserID = value.(string)
		} else if key == "Name" {
			LineName = value.(string)
		} else if key == "GroupID" {
			GroupID = value.(string)
		} else if key == "RoomID" {
			RoomID = value.(string)
		} else if key == "EventMsg" {
			EventMsg = value.(string)
		} else if key == "LogEventMsg" {
			LogEventMsg = value.(string)
		}
	}
	log.Println("UserID Val: ", string(UserID), " - LineName Val: ", string(LineName), " - GroupID Val: ", string(GroupID), " - RoomID Val: ", string(RoomID), " - EventMsg Val: ", string(EventMsg), " - LogEventMsg Val: ", string(LogEventMsg))

	count, err, userDataStr := dsUser.GetCheckUserAuthenStatusDAO(UserID)
	log.Println("count : ", count, " - err : ", err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return userDataStr
}

func GetCheckUserAuthenStatusBLLTest(jsonStr string) (string, int) {
	log.Println()
	log.Println(string("=== GetCheckUserAuthenStatusBLLTest() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var LineName, UserID string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "uid" {
			UserID = value.(string)
		} else if key == "name" {
			LineName = value.(string)
		}
	}
	log.Println("UserID Value : ", string(UserID), " - LineName Value : ", string(LineName))

	count, err, userDataStr, userAuthenStatus := dsUser.GetCheckUserAuthenStatusDAOTest(UserID)
	log.Println("count : ", count, " - err : ", err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return userDataStr, userAuthenStatus
}

//========================================================================================//
/* #endregion */

/* #region REGISTER_LINE_CHATBOT_USER */
//============================ METHOD FOR REGISTER LINE CHATBOT USER PROFILE ============================//
func RegisterLineChatbotUserBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RegisterLineChatbotUserBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _ObjectID, _AuthenStatus int
	var _UserID, _Name, _Department, _Position, _Email, _LogEventMsg string
	_ObjectID = 0
	_ = _ObjectID
	_AuthenStatus = 1
	_ = _AuthenStatus
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "Name" {
			_Name = value.(string)
		} else if key == "Department" {
			_Department = value.(string)
		} else if key == "Position" {
			_Position = value.(string)
		} else if key == "Email" {
			_Email = value.(string)
		} else if key == "LogEventMsg" {
			_LogEventMsg = value.(string)
		}
	}

	//var _UserRegsiterStruct modelUser.UserRegister
	var _UserRegister = modelUser.UserRegister{ObjectID: 0, UserID: _UserID, Name: _Name, Department: _Department, Position: _Position, Email: _Email, AuthenStatus: _AuthenStatus, LogEventMsg: _LogEventMsg}

	log.Println("UserID : ", string(_UserID), " - Name : ", string(_Name), " - Department : ", string(_Department), " - Position : ", string(_Position), " - Email : ", string(_Email))
	log.Println("LogEventMsg : ", string(_LogEventMsg))

	count, err, userDataStr := dsUser.RegisterLineChatbotUserDAO(_UserRegister)
	log.Println("count : ", count, " - err : ", err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return userDataStr
}

//========================================================================================//
/* #endregion */

/* #region UNLOCK_USER_MEMBER */
//============================ METHOD FOR UNLOCK USER MEMBER ============================//
func RequestUserUnLockBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== RequestUserUnLockBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	//var _ObjectID, _AuthenStatus int
	var _UserID, _Reason string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "Reason" {
			_Reason = value.(string)
		}
	}

	//var _UserRegister = modelUser.UserRegister{ObjectID: 0, UserID: _UserID, Name: _Name, Department: _Department, Position: _Position, Email: _Email, AuthenStatus: _AuthenStatus, LogEventMsg: _LogEventMsg}
	log.Println("UserID : ", string(_UserID))

	count, err, userDataStr := dsUser.RequestUserUnLockDAO(_UserID, _Reason)
	log.Println("count : ", count, " - err : ", err)
	_ = userDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return userDataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_ALL_LINE_USER_AUTHEN */
//============================ METHOD FOR GET ALL LINE USER AUTHEN ============================//
func GetUserAuthenAllBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetUserAuthenAllBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		}
	}
	log.Println("UserID : ", string(_UserID))
	count, err, resDataStr := dsUser.GetUserAuthenAllDAO(_UserID)
	log.Println("count : ", count, " - err : ", err)
	_ = resDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return resDataStr
}

//========================================================================================//
/* #endregion */

/* #region GET_ALL_LINE_USER_AUTHEN */
//============================ METHOD FOR GET ALL LINE USER AUTHEN ============================//
func GetUserAuthenByIdBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== GetUserAuthenByIdBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID string
	var _ObjectID string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "ObjectID" {
			_ObjectID = value.(string)
		}
	}
	log.Println("UserID : ", string(_UserID), " ObjectID : ", string(_ObjectID))
	_OIDInt, err := strconv.Atoi(_ObjectID)

	count, err, resDataStr := dsUser.GetUserAuthenByIdDAO(_UserID, _OIDInt)
	log.Println("count : ", count, " - err : ", err)
	_ = resDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return resDataStr
}

//========================================================================================//
/* #endregion */

/* #region UPDATE_USER_PROFILE_STATUS */
//============================ METHOD FOR UPDATE USER PROFILE STATUS ============================//
func UpdateUserProfileStatusBLL(jsonStr string) string {
	log.Println()
	log.Println(string("=== UpdateUserProfileStatusBLL() ==="))
	start := time.Now()
	r := new(big.Int)
	log.Println("Begin Main Start Time ", start)
	log.Println(r.Binomial(1000, 10))

	e := echo.New()
	//e.Logger.SetLevel(99)
	_ = e

	lineReqMap := make(map[string]interface{})
	err3 := json.Unmarshal([]byte(jsonStr), &lineReqMap)

	if err3 != nil {
		panic(err3)
	}
	var _UserID, _ObjectID, _StatusType, _StatusText string
	for key, value := range lineReqMap {
		log.Println("index : ", key, " - value : ", value)
		if key == "UserID" {
			_UserID = value.(string)
		} else if key == "ObjectID" {
			_ObjectID = value.(string)
		} else if key == "StatusType" {
			_StatusType = value.(string)
		} else if key == "StatusText" {
			_StatusText = value.(string)
		}
	}
	log.Println("UserID : ", string(_UserID), ", ObjectID : ", string(_ObjectID), ", StatusType : ", string(_StatusType), ", StatusText : ", string(_StatusText))
	_OIDInt, err := strconv.Atoi(_ObjectID)
	_StatusTypeInt, err := strconv.Atoi(_StatusType)

	count, err, resDataStr := dsUser.UpdateUserProfileStatusDAO(_UserID, _OIDInt, _StatusTypeInt, _StatusText)
	log.Println("count : ", count, " - err : ", err)
	_ = resDataStr
	elapsed := time.Since(start)
	log.Println("End Main Run took ", elapsed)
	return resDataStr
}

//========================================================================================//
/* #endregion */
