package main

import (
	//"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	//"encoding/json"  //import package json.NewEncode

	//departmentDs "../supalai-ess/src/api/v1/department/data/datasource"
	repository "../gme_chatbot_api_core/src/api/v1/user/controller/repository"
	//ds "../gme_chatbot_api_core/src/api/v1/user/model"
	//"gme/linechatbot/api"
)

func main() {
	//http.HandleFunc("/", home)
	//log.Fatal(http.ListenAndServe(":8080", nil)
	handleRequest()
}

//============================== FUNCTION LISTENING HTTP ==============================//
func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	fmt.Println()
	fmt.Println(r.Method)
	fmt.Println()

	//==============>
	//===> ถ้าเป็น Chrome/Opera จะโดน Request 2 รอบ => 1.URL ตามที่ Request, 2.URL /favicon.ico
	//===> ถ้าเป็น IE/Firefox จะโดน Request 1 รอบ => 1.URL ตามที่ Request

	strUrl := r.URL

	fmt.Println(strUrl)
	url := r.URL.String()
	fmt.Println("GET params were:", r.URL.Query())
	/*
		keys, ok := r.URL.Query()["key"]
		if !ok || len(keys[0]) < 1 {
			log.Println("Url Param 'key' is missing")
			return
		}
		// Query()["key"] will return an array of items,
		// we only want the single item.
		key := keys[0]
		log.Println("Url Param 'key' is: " + string(key))
	*/
	//==============>

	switch r.Method {
	/*
		case "GET":
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(`{"message": "get called"}`))
	*/
	case "GET":
		if url != "/favicon.ico" {
			//departmentDataStr := GetAllDepartmentBLL()
			w.WriteHeader(http.StatusOK)
			//w.Write([]byte(departmentDataStr))
			handleRequest()
		}

	case "POST":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "post called"}`))

		/* 		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
		    fmt.Fprintf(w, "ParseForm() err: %v", err)
		    return
		}
		fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
		name := r.FormValue("name")
		address := r.FormValue("address")
		fmt.Fprintf(w, "Name = %s\n", name)
		fmt.Fprintf(w, "Address = %s\n", address) */
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "put called"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "delete called"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "not found"}`))
	}
}

func handleRequest() { // (3)

	// h := &api.Handler{}

	// h.Config = GetConfig(path, filename)
	// h.db = GetDB(connectionstring)

	// r.Add(POST, user(h))

	//==>(3) method handleRequest จะทำหน้าที่รับทุก Request ที่ Client ส่งผ่านมาเข้าสู่ Server เราและ จะแยกว่ามันจะไป Path ไหน
	http.HandleFunc("/", homePage)          // (4)
	http.HandleFunc("/getUser", getUserAll) // เพิ่มบรรทัดนี้

	http.HandleFunc("/getUserAuthenAllPost", GetUserAuthenAllPost)                       //===> Get All Line User Authen
	http.HandleFunc("/getUserAuthenByIdPost", GetUserAuthenByIdPost)                     //===> Get Line User Authen By ObjectID
	http.HandleFunc("/checkUserPost", GetCheckUserAuthenStatusPost)                      //===> Check User AuthenStatus Or None Register
	http.HandleFunc("/checkUserPostTest", GetCheckUserAuthenStatusPostTest)              //===> Check User AuthenStatus Or None Register (Test)
	http.HandleFunc("/registerUserPost", RegisterLineChatbotUserPost)                    //===> สำหรับ Register User ผ่านเวป ที่ทำแยกด้วย React
	http.HandleFunc("/requestUserUnLockPost", RequestUserUnLockPost)                     //===> สำหรับ Unlock User ผ่านเวป ที่ทำแยกด้วย React
	http.HandleFunc("/requestMailImapPost", RequestMailImapPost)                         //===> สำหรับ Get Mail ที่ UnRead เพื่อ Insert ลง Database ที่ทำแยกด้วย React
	http.HandleFunc("/requestLastMailByEventMsgPost", RequestLastMailDataByEventMsgPost) //===> สำหรับ Get ค่า Event ของ Mail ล่าสุดที่อยู่ใน Database ของ Batch Job ที่ Post เข้ามา
	http.HandleFunc("/requestAllMailByEventMsgPost", RequestAllMailDataByEventMsgPost)   //===> สำหรับ Get ค่า Event ของ Mail ทั้งหมดที่อยู่ใน Database ของ Batch Job ที่ Post เข้ามา
	http.HandleFunc("/requestBatchLogSendMailPost", RequestBatchLogSendMailPost)
	http.HandleFunc("/updateUserProfileStatusPost", UpdateUserProfileStatusPost)          //===> สำหรับ Update User Status By Web Admin (Active, Lock, UnLock)
	http.HandleFunc("/getMessageBatchLogFlexPost", GetMessageBatchLogFlexPost)            //===> สำหรับ Get Info Batch By Mapping EventMsg
	http.HandleFunc("/getDetailBatchGLByBatchNamePost", GetDetailBatchGL_ByBatchNamePost) //===> สำหรับ Get Detail Batch GL

	//==>(4) http.HandleFunc คือ เมื่อมี Request เข้ามาแล้วมันจับคู่ได้ Path “/” มันจะส่ง Request ผ่านไปต่อที่ method homePage ให้ทำงานต่อทันที
	http.ListenAndServe(":7000", nil) // (5)
	//==>(5) http.ListenAndServe รับ Parameter 2 ตัว คือ
	//		 Port ที่เราต้องการให้ Request จาก Client ส่งเข้ามา ณ ที่นี้คือ Port 8080
	//		 Option สำหรับเลือกใช้ตัว Handle Request ถ้าใส่เป็น nil มันจะใช้ DefaultServeMux ในการ Handle Request
	log.Fatal(http.ListenAndServe(":7000", nil))
}

func homePage(w http.ResponseWriter, r *http.Request) { // (1)
	//==>(1) method homePage รับ Parameter 2 ตัว คือ http.ResponseWriter และ http.Request
	// 		 http.ResponseWriter คือ ค่าบางอย่างที่เราจะต้องการส่งออกกลับไปยัง Client
	//		 http.Request คือ ข้อมูล Request ที่ Client ส่งเข้ามาให้เรา
	fmt.Fprint(w, "Welcome to the HomePage!") // (2)
	//==>(2) ซึ่ง ณ ที่นี้ เราจะส่ง Welcome To the HomePage! ออกไป ในกรณี มี Request เรียกหน้า homePage เรา
}

func chkRequest(r *http.Request) bool {
	var _chkUrlPath bool = true
	strUrl := r.URL
	fmt.Println(strUrl)
	url := r.URL.String()
	fmt.Println("GET URL:", url)
	fmt.Println("GET params were:", r.URL.Query())
	if url == "/favicon.ico" {
		//_chkUrlPath = false
		_chkUrlPath = true
	}
	return _chkUrlPath
}

func getUserAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	fmt.Println(string("=== getDepartmentAll() ==="))
	userDataStr := repository.GetAllUserBLL()
	//json.NewEncoder(w).Encode(personMap) // (1) //===> กรณีที่เป็น Struct
	w.WriteHeader(http.StatusOK)
	//w.Write([]byte(`{"message": "get called"}`))
	w.Write([]byte(userDataStr))
}

/* #region GET_ALL_LINE_USER_AUTHEN */
func GetUserAuthenAllPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetUserAuthenAllPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		resDataStr := repository.GetUserAuthenAllBLL(jsonStr)
		_ = resDataStr
		w.Write([]byte(resDataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}
}

/* #region GET_LINE_USER_AUTHEN_BY_ID */
func GetUserAuthenByIdPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetUserAuthenByIdPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		resDataStr := repository.GetUserAuthenByIdBLL(jsonStr)
		_ = resDataStr
		w.Write([]byte(resDataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}
}

/* #region CHECK_USER_STATUS_AuthenStatus */
func GetCheckUserAuthenStatusPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetCheckUserAuthenStatusPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		// type teststruct struct {
		// 	uid  string `json:"uid"`
		// 	name string `json:"name"`
		// }

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))

		//data := teststruct{}
		jsonStr := string(body)
		userDataStr := repository.GetCheckUserAuthenStatusBLL(jsonStr)
		_ = userDataStr
		w.Write([]byte(userDataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

func GetCheckUserAuthenStatusPostTest(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetCheckUserAuthenStatusPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		type teststruct struct {
			uid  string `json:"uid"`
			name string `json:"name"`
		}
		//====> ใช้ได้
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		userDataStr, userAuthenStatus := repository.GetCheckUserAuthenStatusBLLTest(jsonStr)
		log.Println(userDataStr)
		log.Println(userAuthenStatus)
		_ = userDataStr
		w.Write([]byte(fmt.Sprintf("%d", userAuthenStatus)))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region CHECK_USER_STATUS_AuthenStatus */
func RegisterLineChatbotUserPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RegisterLineChatbotUserPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		type teststruct struct {
			uid  string `json:"uid"`
			name string `json:"name"`
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))

		//data := teststruct{}
		jsonStr := string(body)
		userDataStr := repository.RegisterLineChatbotUserBLL(jsonStr)
		_ = userDataStr
		w.Write([]byte(userDataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region CHECK_USER_STATUS_AuthenStatus */
func RequestUserUnLockPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestUserUnLockPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		type teststruct struct {
			uid  string `json:"uid"`
			name string `json:"name"`
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))

		//data := teststruct{}
		jsonStr := string(body)
		userDataStr := repository.RequestUserUnLockBLL(jsonStr)
		_ = userDataStr
		w.Write([]byte(userDataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GET_MAIL_WITH_IMAP */
func RequestMailImapPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestMailImapPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.RequestMailImapBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GET_MAIL_DATA_FROMDATABASE_LAST_EVENT */
func RequestLastMailDataByEventMsgPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestLastMailDataByEventMsgPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.RequestLastMailDataByEventMsgBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GET_MAIL_DATA_FROMDATABASE_ALL_EVENT */
func RequestAllMailDataByEventMsgPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestAllMailDataByEventMsgPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.RequestAllMailDataByEventMsgBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GET_DATA_FOR_SENDMAIL_LOG_BATCH */
func RequestBatchLogSendMailPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== RequestBatchLogSendMailPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.RequestBatchLogSendMailBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region UPDATE_USER_PROFILE_STATUS */
func UpdateUserProfileStatusPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== UpdateUserProfileStatusPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.UpdateUserProfileStatusBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GetMessageBatchLogFlexPost */
func GetMessageBatchLogFlexPost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetMessageBatchLogFlexPost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.GetDataMappingByEventMsgBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */

/* #region GetDetailBatchGL_ByBatchNamePost */
func GetDetailBatchGL_ByBatchNamePost(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	log.Println(string("=== GetDetailBatchGL_ByBatchNamePost() ==="))
	log.Println(r.Method)
	strUrl := r.URL
	log.Println(strUrl)
	//url := r.URL.String()
	log.Println("GET params were:", r.URL.Query())
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Get"}`))
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string("body"), string(body))
		jsonStr := string(body)
		dataStr := repository.GetDetailBatchGL_ByBatchNameBLL(jsonStr)
		_ = dataStr
		w.Write([]byte(dataStr))
		//w.WriteHeader(http.StatusCreated)
		//w.Write([]byte(`{"message": "post called"}`))
	case "PUT":
		w.WriteHeader(http.StatusAccepted)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Put"}`))
	case "DELETE":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "This URL Not Allowed Call Method Delete"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "This URL Not Found"}`))
	}

}

/* #endregion */
